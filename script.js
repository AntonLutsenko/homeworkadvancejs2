const books = [
	{
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70,
	},
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	},
	{
		name: "Дизайн. Книга для недизайнерів.",
		price: 70,
	},
	{
		author: "Алан Мур",
		name: "Неономікон",
		price: 70,
	},
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40,
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	},
];

const list = document.createElement("ul");
let container = document.querySelector("#root");
container.append(list);

const propertyCheck = ["author", "name", "price"];
const approveBooks = [];

function showListBooks(arr) {
	const arrList = [];
	arr.forEach((item) => {
		let stringBooks;
		stringBooks = `<li>Автор: ${item.author}, Назва: "${item.name}", Ціна: ${item.price} грн..</li>`;
		arrList.push(stringBooks);
	});
	list.insertAdjacentHTML("beforeend", `${arrList.join("")}`);
}

function createListBooks(arr, propertyCheck) {
	Object.entries(arr).map((elem, index) => {
		try {
			propertyCheck.forEach((prop) => {
				if (!elem[1].hasOwnProperty(prop)) {
					throw `Помилкаб відсутня властивість  ${prop}, індекс книги: ${index}`;
				}
			});
			approveBooks.push(elem[1]);
			return approveBooks;
		} catch (error) {
			console.log(error);
		}
	});
	showListBooks(approveBooks);
}

createListBooks(books, propertyCheck);
